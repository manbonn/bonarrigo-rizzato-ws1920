# Famiglia Verdi

city: Roma
address: via dei pini 50
state: Italy

- Paolo Verdi nato Bianchi M "1976-01-22T00:00:00"^^xsd:dateTime
- Margherita Verdi F "1978-04-02T00:00:00"^^xsd:dateTime
- Sofia Verdi F "2002-08-01T00:00:00"^^xsd:dateTime
    - figlia
- Rebecca Verdi F "1999-12-30T00:00:00"^^xsd:dateTime
    - figlia

# Famiglia Rossi

city: Roma
address: via delle palme 69
state: Italy

- Antonio Rossi M "1960-06-22T00:00:00"^^xsd:dateTime
- Maria Rossi nata Esposito F "1965-04-24T00:00:00"^^xsd:dateTime
- Alessandro Rossi M "1996-09-09T00:00:00"^^xsd:dateTime
    - figlio
    - hearing impairment

# Famiglia Neri

city: Roma
address: via XXV Aprile 776
state: Italy

- Nerio Neri M "1980-11-11T00:00:00"^^xsd:dateTime
- Marco Neri nato Brambilla M "1977-10-06T00:00:00"^^xsd:dateTime