package client.view;

import static client.Client.ClientMonitorPosition;
import client.control.ClientControl;
import domain.OwlDataTransfer;
import org.semanticweb.owlapi.model.*;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClientSwingView extends JFrame implements ClientView  {

    private static final double WIDTH_PERCENTAGE = 0.3;
    private static final double HEIGHT_PERCENTAGE = 0.5;

    private static final Rectangle MONITOR =
            GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getDefaultScreenDevice()
                .getDefaultConfiguration()
                .getBounds();

    private static final int WIDTH = (int) ( MONITOR.width * WIDTH_PERCENTAGE);
    private static final int HEIGHT = (int) ( MONITOR.height * HEIGHT_PERCENTAGE);

    private final String clientId;
    private final ClientControl controller;

    private JEditorPane review = new JEditorPane();

    public ClientSwingView(final String clientId, final ClientControl controller, final ClientMonitorPosition position){
        this.clientId = clientId;
        this.controller = controller;

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setSize(WIDTH, HEIGHT);
        this.setTitle("Device client " + this.clientId);

        this.getContentPane().add(initViewStructure());
        this.setVisible(true);
        locatePosition(position);
        previewNextInfected();
    }

    private JPanel initViewStructure() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        review = new JEditorPane();
        final JButton nextEvent = new JButton("Next Event");
        review.setEditable(false);

        final Dimension textAreaDimension = new Dimension(WIDTH, HEIGHT - 100);
        final Dimension buttonDimension = new Dimension(WIDTH, 50);

        panel.setAlignmentY(Component.CENTER_ALIGNMENT);

        nextEvent.setSize(buttonDimension);
        nextEvent.setPreferredSize(buttonDimension);
        nextEvent.setMaximumSize(buttonDimension);

        review.setSize(textAreaDimension);
        review.setPreferredSize(textAreaDimension);
        review.setMaximumSize(textAreaDimension);
        review.setBorder(
                BorderFactory.createTitledBorder("User's Information"));

        panel.add(Box.createRigidArea(new Dimension(0, 8)));
        panel.add(review, BorderLayout.NORTH);
        panel.add(Box.createRigidArea(new Dimension(0, 8)));
        panel.add(nextEvent, BorderLayout.SOUTH);
        panel.add(Box.createRigidArea(new Dimension(0, 8)));

        this.revalidate();

        nextEvent.addActionListener(e -> handleClick((JButton) e.getSource()));

        return panel;
    }

    private void handleClick(final JButton source) {
        controller.fireNextDataTransferObject();
        previewNextInfected();
    }

    private void previewNextInfected() {
        final Optional<OwlDataTransfer> dto = controller.previewNextInfected();
        dto.ifPresent(owl -> {
            review.setText(beautifyDTO(owl));
        });
    }

    private String beautifyDTO(OwlDataTransfer dto) {
        review.setBorder(null);

        String result = "";

        java.util.List<OWLDeclarationAxiom> individual = dto.getIndividuals();
        java.util.List<OWLClassAssertionAxiom> clazz = dto.getClassAssertions();
        java.util.List<OWLDataPropertyAssertionAxiom> data = dto.getDataProperties();
        java.util.List<OWLObjectPropertyAssertionAxiom> object = dto.getObjectProperties();

        for (OWLClassAssertionAxiom ax : clazz) {
            final String partial =
                    ax.signature()
                    .map(OWLEntity::toString)
                    .map(str -> str.substring(str.indexOf("#") + 1))
                    .map(str -> str.substring(0, str.length() - 1))
                    .collect(Collectors.joining(": "));

            result = result.concat(partial + "\n");
        }

        result = result.concat("\n");

        for (OWLDataPropertyAssertionAxiom ax : data) {
            final List<String> collected = ax.components().map(Object::toString).collect(Collectors.toList());
            String partial1 = collected.get(0);
            partial1 = partial1.substring(partial1.indexOf("#") + 1, partial1.length() - 1);
            String partial2 = collected.get(1);
            partial2 = partial2.substring(partial2.indexOf("#") + 1, partial2.length() - 1);
            String partial3 = collected.get(2);
            partial3 = partial3.substring(partial3.indexOf("\"") + 1, partial3.lastIndexOf("\""));

            result = result.concat(partial1 + " " + partial2 + " " + partial3 + "\n");
        }

        if(result.contains("Positive")) {
            review.setBorder(BorderFactory.createLineBorder(Color.RED));
        }

        return result;
    }

    private void locatePosition(final ClientMonitorPosition position) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
        Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();

        switch(position){
            case TOP_LEFT: {
                int x = 0;
                int y = 0;
                this.setLocation(x, y);
                break;
            }
            case TOP_RIGHT: {
                int x = (int) rect.getMaxX() - this.getWidth();
                int y = 0;
                this.setLocation(x, y);
                break;
            }
            case BOTTOM_LEFT: {
                int x = 0;
                int y = (int) rect.getMaxY() - this.getHeight();
                this.setLocation(x, y);
                break;
            }
            case BOTTOM_RIGHT: {
                int x = (int) rect.getMaxX() - this.getWidth();
                int y = (int) rect.getMaxY() - this.getHeight();
                this.setLocation(x, y);
                break;
            }
        }
    }
}
