package client;

import client.control.ClientControl;
import client.control.ClientControlImpl;
import client.model.ClientModel;
import client.model.ClientOwlApiModel;
import client.view.ClientSwingView;
import client.view.ClientView;

public class Client {

    public static void main(String[] args) {
        final String id = args[0];
        final String graph = args[1];
        final ClientMonitorPosition position = decodePositionalArgument(args[2]);

        final ClientModel model = new ClientOwlApiModel(id, graph);
        final ClientControl controller = new ClientControlImpl(id, model);
        final ClientView view = new ClientSwingView(id, controller, position);
    }

    private static ClientMonitorPosition decodePositionalArgument(final String position){
        switch (position){
            case "topleft": {
                return ClientMonitorPosition.TOP_LEFT;
            }
            case "topright": {
                return ClientMonitorPosition.TOP_RIGHT;
            }
            case "bottomleft": {
                return ClientMonitorPosition.BOTTOM_LEFT;
            }
            case "bottomright": {
                return ClientMonitorPosition.BOTTOM_RIGHT;
            }
            default: {
                throw new IllegalArgumentException("Only topleft, topright, bottomleft and bottomright values are accepted");
            }
        }
    }

    public enum ClientMonitorPosition {
        TOP_LEFT, BOTTOM_LEFT, TOP_RIGHT, BOTTOM_RIGHT;
    }

}
