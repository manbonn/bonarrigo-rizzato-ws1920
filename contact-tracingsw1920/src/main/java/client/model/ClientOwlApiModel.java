package client.model;

import domain.OwlDataTransfer;
import domain.OwlDataTransferImpl;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.owlxml.parser.OWLXMLParser;

import java.io.File;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ClientOwlApiModel implements ClientModel {

    private static final String DTO_SEPARATOR = "%%END_OF_INDIVIDUAL%%";

    private static final List<String> XML_PREAMBLE = Arrays.asList(
        "<?xml version=\"1.0\"?>",
        "<Ontology>",
        "    <Prefix name=\"ct\" IRI=\"http://www.semanticweb.org/bonarrigo-rizzato/ontologies/2020/5/contact-tracing#\"/>",
        "    <Prefix name=\"po\" IRI=\"http://blankdots.com/open/personasonto.owl#\"/>",
        "    <Prefix name=\"owl\" IRI=\"http://www.w3.org/2002/07/owl#\"/>",
        "    <Prefix name=\"rdf\" IRI=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"/>",
        "    <Prefix name=\"xml\" IRI=\"http://www.w3.org/XML/1998/namespace\"/>",
        "    <Prefix name=\"xsd\" IRI=\"http://www.w3.org/2001/XMLSchema#\"/>",
        "    <Prefix name=\"rdfs\" IRI=\"http://www.w3.org/2000/01/rdf-schema#\"/>"
    );

    private static final String XML_EPILOGUE = "</Ontology>";

    private final OWLOntologyManager manager;
    private final OWLXMLParser parser;
    private final File source;
    private File temporaryFile;

    public ClientOwlApiModel(final String clientId, final String path){
        this.manager = OWLManager.createOWLOntologyManager();
        this.parser = new OWLXMLParser();

        Path sourceCloneFilePath = Paths.get(Paths.get(path).getParent().toString(), clientId + "sourceClone.ct");
        Path temporaryFilePath = Paths.get(Paths.get(path).getParent().toString(), clientId + "temp.xml");
        this.temporaryFile = new File(temporaryFilePath.toString());
        this.source = new File(sourceCloneFilePath.toString());
        source.deleteOnExit();
        temporaryFile.deleteOnExit();
        cloneSource(new File(path), source);
    }

    private void cloneSource(final File source, final File sourceClone) {
        try {
            final Scanner sourceScanner = new Scanner(source);
            final PrintStream clonePrinter = new PrintStream(sourceClone);

            while(sourceScanner.hasNextLine()) {
                clonePrinter.println(sourceScanner.nextLine());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<OwlDataTransfer> getNextDataTransferObject() {
        if(temporaryFile!=null){
            updateFileContent();
        }
        if(temporaryFile!=null) {
            return Optional.ofNullable(createDataTransferObject());
        } else {
            return Optional.empty();
        }
    }

    private void updateFileContent() {
        try {
            final Scanner sourceScanner = new Scanner(source);
            final PrintStream tempPrinter = new PrintStream(temporaryFile);
            final List<String> temp = new ArrayList<>();
            boolean individualEndIsMet = false;

            if(!sourceScanner.hasNextLine()){
                temporaryFile = null;
                return;
            }

            XML_PREAMBLE.forEach(tempPrinter::println);
            while(sourceScanner.hasNextLine()) {
                final String line = sourceScanner.nextLine();
                if(individualEndIsMet){
                    temp.add(line);
                } else {
                    if(line.equals(DTO_SEPARATOR)) {
                        individualEndIsMet = true;
                    } else {
                        tempPrinter.println(line);
                    }
                }
            }
            tempPrinter.println(XML_EPILOGUE);
            tempPrinter.close();
            sourceScanner.close();

            final PrintStream printer = new PrintStream(source);
            temp.forEach(printer::println);
            printer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private OwlDataTransfer createDataTransferObject() {
        try {
            final OWLOntology ontology;

            ontology = manager.createOntology();
            IRI iri = IRI.create(temporaryFile);
            parser.parse(iri, ontology);

            List<OWLDeclarationAxiom> individual = new ArrayList<>();
            List<OWLClassAssertionAxiom> clazz = new ArrayList<>();
            List<OWLDataPropertyAssertionAxiom> data = new ArrayList<>();
            List<OWLObjectPropertyAssertionAxiom> object = new ArrayList<>();

            for(OWLNamedIndividual i : ontology.individualsInSignature().collect(Collectors.toList())) {
                individual.addAll(ontology.declarationAxioms(i).collect(Collectors.toList()));
                clazz.addAll(ontology.classAssertionAxioms(i).collect(Collectors.toList()));
                data.addAll(ontology.dataPropertyAssertionAxioms(i).collect(Collectors.toList()));
                object.addAll(ontology.objectPropertyAssertionAxioms(i).collect(Collectors.toList()));
            }

//            individual.stream().forEach(System.out::println);
//            clazz.stream().forEach(System.out::println);
//            data.stream().forEach(System.out::println);
//            object.stream().forEach(System.out::println);

            return new OwlDataTransferImpl(individual, clazz, data, object);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

}

