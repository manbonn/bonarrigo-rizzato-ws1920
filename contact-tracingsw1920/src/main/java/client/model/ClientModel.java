package client.model;

import domain.Infected;
import domain.OwlDataTransfer;

import java.util.Optional;

public interface ClientModel {

    Optional<OwlDataTransfer> getNextDataTransferObject();

}
