package client.control;

import client.view.ClientView;
import client.model.ClientModel;
import domain.OwlDataTransfer;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Optional;

public class ClientControlImpl implements ClientControl{

    private final String clientId;
    private final ClientModel model;
    private ClientView view;
    private Optional<OwlDataTransfer> nextData;

    public ClientControlImpl (final String clientId, final ClientModel model) {
        this.clientId = clientId;
        this.model = model;
    }

    @Override
    public void fireNextDataTransferObject() {
        nextData.ifPresent(dto -> new Thread(new CommunicationAgent(dto)).start());
    }

    @Override
    public void attachView(ClientView view) {
        this.view = view;
    }

    @Override
    public Optional<OwlDataTransfer> previewNextInfected() {
        nextData =  model.getNextDataTransferObject();
        return nextData;
    }

    private static class CommunicationAgent implements Runnable{

        private final static String HOST = "localhost";
        private final static int PORT = 8888;

        private final OwlDataTransfer dto;

        public CommunicationAgent(final OwlDataTransfer dto) {
            this.dto = dto;
        }

        @Override
        public void run() {
            try {
                Socket socket = new Socket(HOST, PORT);
                ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
                output.writeObject(this.dto);
                output.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
