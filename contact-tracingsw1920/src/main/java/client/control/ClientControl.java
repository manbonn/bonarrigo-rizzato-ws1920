package client.control;

import client.view.ClientView;
import domain.OwlDataTransfer;

import java.util.Optional;

public interface ClientControl {

    void fireNextDataTransferObject();

    void attachView(ClientView view);

    Optional<OwlDataTransfer> previewNextInfected();

}
