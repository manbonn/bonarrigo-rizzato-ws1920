import client.Client;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Client1 {

    private final static String CLIENT_ID = "1";
    private final static String CLIENT_POSITION = "topleft";
    private final static Path RDF =
            Paths.get(System.getProperty("user.dir"), "src", "main", "resources", "sourceClient1.ct");
//            Paths.get(System.getProperty("user.dir"), "src", "main", "resources", "aa.xml");

    public static void main(String[] args) {
        final String[] firstClientArgs = new String[] {CLIENT_ID, RDF.toString(), CLIENT_POSITION};
        Client.main(firstClientArgs);
    }

}
