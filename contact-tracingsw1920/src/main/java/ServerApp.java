import server.Server;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ServerApp {

    private final static String SERVER_POSITION = "right";
    private final static Path ONTOLOGY =
            Paths.get(System.getProperty("user.dir"), "src", "main", "resources", "contact-tracing.owl");

    public static void main(String[] args) {
        final String[] serverArgs = new String[] {ONTOLOGY.toString(), SERVER_POSITION};
        Server.main(serverArgs);
    }
}
