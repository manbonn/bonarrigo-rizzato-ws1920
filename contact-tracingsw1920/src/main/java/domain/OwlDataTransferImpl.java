package domain;

import org.semanticweb.owlapi.model.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OwlDataTransferImpl implements OwlDataTransfer, Serializable {

    private final List<OWLDeclarationAxiom> declaration;
    private final List<OWLClassAssertionAxiom> classAssertions;
    private final List<OWLDataPropertyAssertionAxiom> dataProperties;
    private final List<OWLObjectPropertyAssertionAxiom> objectProperties;

    public OwlDataTransferImpl(
            final List<OWLDeclarationAxiom> declaration,
            final List<OWLClassAssertionAxiom> classAssertions,
            final List<OWLDataPropertyAssertionAxiom> dataProperties,
            final List<OWLObjectPropertyAssertionAxiom> objectProperties) {
        this.declaration = new ArrayList<>(declaration);
        this.classAssertions = new ArrayList<>(classAssertions);
        this.dataProperties = new ArrayList<>(dataProperties);
        this.objectProperties = new ArrayList<>(objectProperties);
    }

    @Override
    public List<OWLDataPropertyAssertionAxiom> getDataProperties() {
        return new ArrayList<>(dataProperties);
    }

    @Override
    public List<OWLObjectPropertyAssertionAxiom> getObjectProperties() {
        return new ArrayList<>(objectProperties);
    }

    @Override
    public List<OWLDeclarationAxiom> getIndividuals() {
        return new ArrayList<>(declaration);
    }

    @Override
    public List<OWLClassAssertionAxiom> getClassAssertions() { return new ArrayList<>(classAssertions); }

    @Override
    public String toString() {
        return declaration.toString() + "\n" + classAssertions.toString() + "\n" + dataProperties.toString() + "\n" + objectProperties.toString() + "\n";
    }
}
