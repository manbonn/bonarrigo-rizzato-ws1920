package domain;

import java.io.Serializable;
import java.util.Date;

public class ExposedSubjectImpl implements ExposedSubject, Serializable {

    private final String name;
    private final String surname;
    private final String telephone;
    private final Date encounterDate;
    private final String encounterType;

    public ExposedSubjectImpl(String name, String surname, String telephone, Date encounterDate, String encounterType){
        this.name = name;
        this.surname = surname;
        this.telephone = telephone;
        this.encounterDate = encounterDate;
        this.encounterType = encounterType;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String surname() {
        return surname;
    }

    @Override
    public String telephone() {
        return telephone;
    }

    @Override
    public Date encounterDate() {
        return encounterDate;
    }

    @Override
    public String encounterType() {return encounterType;}
}
