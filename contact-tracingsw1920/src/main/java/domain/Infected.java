package domain;

import java.util.Date;
import java.util.List;

public interface Infected {

    String name();

    String surname();

    String testType();

    Date testDate();

    Date diagnosisDate();

    List<ExposedSubject> circle();

}
