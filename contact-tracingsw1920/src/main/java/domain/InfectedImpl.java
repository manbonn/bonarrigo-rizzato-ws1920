package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InfectedImpl implements Infected, Serializable {

    private final String name;
    private final String surname;
    private final String testType;
    private final Date testDate;
    private final Date diagnosisDate;
    private final List<ExposedSubject> circle;

    public InfectedImpl(String name,
                        String surname,
                        String testType,
                        Date testDate,
                        Date diagnosisDate,
                        List<ExposedSubject> circle) {
        this.name = name;
        this.surname = surname;
        this.testType = testType;
        this.testDate = testDate;
        this.diagnosisDate = diagnosisDate;
        this.circle = new ArrayList<>(circle);
    }

    public InfectedImpl(String name, String surname, String testType, Date testDate, Date diagnosisDate){
        this(name, surname, testType, testDate, diagnosisDate, new ArrayList<>());
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String surname() {
        return surname;
    }

    @Override
    public String testType() {
        return testType;
    }

    @Override
    public Date testDate() {
        return testDate;
    }

    @Override
    public Date diagnosisDate() {
        return diagnosisDate;
    }

    @Override
    public List<ExposedSubject> circle() {
        return new ArrayList<>(circle);
    }
}
