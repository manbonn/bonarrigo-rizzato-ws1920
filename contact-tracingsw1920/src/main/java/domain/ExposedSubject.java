package domain;

import java.util.Date;

public interface ExposedSubject {

    String name();

    String surname();

    String telephone();

    Date encounterDate();

    String encounterType();


}
