package domain;

import org.semanticweb.owlapi.model.*;

import java.util.List;

public interface OwlDataTransfer {

    List<OWLDeclarationAxiom> getIndividuals();

    List<OWLClassAssertionAxiom> getClassAssertions();

    List<OWLObjectPropertyAssertionAxiom> getObjectProperties();

    List<OWLDataPropertyAssertionAxiom> getDataProperties();

}
