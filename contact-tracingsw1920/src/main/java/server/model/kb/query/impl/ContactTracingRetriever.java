package server.model.kb.query.impl;

import org.semanticweb.owlapi.model.*;
import server.model.kb.KnowledgeBaseImpl;
import server.model.kb.Namespace;
import server.model.kb.query.AbstractQueryEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ContactTracingRetriever extends AbstractQueryEngine {

    private static final String NAMESPACE = Namespace.CONTACT_TRACING_NAMESPACE.IRI;

    public ContactTracingRetriever(KnowledgeBaseImpl knowledgeBase) {
        super(knowledgeBase);
    }

    public List<OWLNamedIndividual> getIndividualsOfClass(String className) {
        final OWLClass owlClass = this.dataFactory.getOWLClass(IRI.create(
                NAMESPACE,
                className
        ));
//        System.out.println(IRI.create(
//                NAMESPACE,
//                className
//        ));
        final List<OWLNamedIndividual> individuals = this.reasoner.getInstances(owlClass)
                .entities()
                .collect(Collectors.toList());

        return individuals;
    }

    public List<OWLNamedIndividual> getObjectPropertyValue(String individualName, String objectPropertyName) {
        final OWLNamedIndividual individual = dataFactory.getOWLNamedIndividual(
                NAMESPACE,
                individualName
        );
        final OWLObjectProperty objectProperty = dataFactory.getOWLObjectProperty(
                NAMESPACE,
                objectPropertyName
        );
        final List<OWLNamedIndividual> propertyValues = reasoner.getObjectPropertyValues(individual, objectProperty)
                .entities()
                .collect(Collectors.toList());

        return propertyValues;
    }

    public List<OWLLiteral> getDataPropertyValue(String individualName, String dataPropertyName) {
        final OWLNamedIndividual individual = dataFactory.getOWLNamedIndividual(
                NAMESPACE,
                individualName
        );
        final OWLDataProperty dataProperty = dataFactory.getOWLDataProperty(
                NAMESPACE,
                dataPropertyName
        );
        final List<OWLLiteral> propertyValues = new ArrayList<>(reasoner.getDataPropertyValues(individual, dataProperty));

        return propertyValues;
    }
}
