package server.model.kb.query.impl;

import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import server.model.kb.KnowledgeBaseImpl;
import server.model.kb.Namespace;
import server.model.kb.query.AbstractQueryEngine;

import java.util.ArrayList;
import java.util.List;

public class PersonaRetriever extends AbstractQueryEngine {

    private static final String CT_NAMESPACE = Namespace.CONTACT_TRACING_NAMESPACE.IRI;
    private static final String PERSONA_NAMESPACE = Namespace.PERSONA_NAMESPACE.IRI;

    public PersonaRetriever(KnowledgeBaseImpl knowledgeBase) {
        super(knowledgeBase);
    }

    public List<OWLLiteral> getPersonaDataPropertyValue(String individualName, String dataPropertyName) {
        final OWLNamedIndividual individual = dataFactory.getOWLNamedIndividual(
                CT_NAMESPACE,
                individualName
        );
        final OWLDataProperty dataProperty = dataFactory.getOWLDataProperty(
                PERSONA_NAMESPACE,
                dataPropertyName
        );
        final List<OWLLiteral> propertyValues = new ArrayList<>(reasoner.getDataPropertyValues(individual, dataProperty));

        return propertyValues;
    }

}
