package server.model.kb.query.impl;

import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.parameters.ChangeApplied;
import server.model.kb.KnowledgeBaseImpl;
import server.model.kb.query.AbstractQueryEngine;

public class OWLAdder extends AbstractQueryEngine {
    public OWLAdder(KnowledgeBaseImpl knowledgeBase) {
        super(knowledgeBase);
    }

    public void addIndividual(final OWLNamedIndividual individual) {
        final OWLDeclarationAxiom declarationAxiom = this.dataFactory.getOWLDeclarationAxiom(individual);
        this.manager.addAxiom(this.ontology, declarationAxiom);
        this.knowledgeBase.updateReasoner();
    }

    public void addAxiom(final OWLAxiom axiom) {
//        System.out.println("Adding axiom: " + axiom);
        final ChangeApplied axiomChange = this.manager.addAxiom(this.ontology, axiom);
//        System.out.println(axiomChange);

        this.knowledgeBase.updateReasoner();
    }

}
