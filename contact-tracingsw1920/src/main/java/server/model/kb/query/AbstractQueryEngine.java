package server.model.kb.query;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import server.model.kb.KnowledgeBaseImpl;

public class AbstractQueryEngine implements QueryEngine {

    protected final KnowledgeBaseImpl knowledgeBase;

    protected OWLDataFactory dataFactory;
    protected OWLReasoner reasoner;
    protected OWLOntologyManager manager;
    protected OWLOntology ontology;

    public AbstractQueryEngine(final KnowledgeBaseImpl knowledgeBase) {
        this.knowledgeBase = knowledgeBase;
        this.knowledgeBase.acceptQueryEngine(this);
    }

    @Override
    public void setOWLDataFactory(OWLDataFactory dataFactory) {
        this.dataFactory = dataFactory;
    }

    @Override
    public void setOWLReasoner(OWLReasoner reasoner) {
        this.reasoner = reasoner;
    }

    @Override
    public void setOntology(OWLOntology ontology) {
        this.ontology = ontology;
    }

    @Override
    public void setOWLManager(OWLOntologyManager manager) {
        this.manager = manager;
    }
}
