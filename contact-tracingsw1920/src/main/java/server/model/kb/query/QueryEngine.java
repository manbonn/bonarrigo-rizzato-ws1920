package server.model.kb.query;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public interface QueryEngine {
    void setOntology(OWLOntology ontology);

    void setOWLManager(OWLOntologyManager manager);

    void setOWLDataFactory(OWLDataFactory dataFactory);

    void setOWLReasoner(OWLReasoner reasoner);
}
