package server.model.kb;

import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.*;
import server.model.kb.query.QueryEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class KnowledgeBaseImpl {

    private final static String destinationFilename = "contact-tracing-inferred.owl";

    private final File source;
    private final File destination;

    private OWLOntology ontology;
    private final OWLOntologyManager manager;
    private OWLReasoner reasoner;
    private OWLDataFactory dataFactory;
    private InferredOntologyGenerator inferredOntologyGenerator;

    public KnowledgeBaseImpl(final String ontologyFilePath) {
        this.source = new File(ontologyFilePath);
        this.manager = OWLManager.createOWLOntologyManager();

        final String destinationPath =
                ontologyFilePath.substring(0, ontologyFilePath.lastIndexOf(File.separator) + 1) + destinationFilename;
        System.out.println(destinationPath);
        destination = new File(destinationPath);

        this.loadOntology();
        this.startReasoner();
        this.setupInferenceTypes();
    }

    public void acceptQueryEngine(final QueryEngine queryEngine) {
        this.visitQueryEngine(queryEngine);
    }

    private void visitQueryEngine(final QueryEngine queryEngine) {
        queryEngine.setOntology(this.ontology);
        queryEngine.setOWLManager(this.manager);
        queryEngine.setOWLDataFactory(this.dataFactory);
        queryEngine.setOWLReasoner(this.reasoner);
    }

    public void addIndividual(final OWLNamedIndividual individual) {
        final OWLDeclarationAxiom declarationAxiom = this.dataFactory.getOWLDeclarationAxiom(individual);
        this.manager.addAxiom(this.ontology, declarationAxiom);
        this.updateReasoner();
    }

    public void addAxiom(final OWLAxiom axiom) {
        this.manager.addAxiom(this.ontology, axiom);
        this.updateReasoner();
    }

    public void flushReasoner() {
        this.reasoner.flush();
    }

    public void updateReasoner() {
        this.inferredOntologyGenerator.fillOntology(this.dataFactory, this.ontology);
        this.checkOntologyConsistency();
    }

    private void checkOntologyConsistency() {
        if (!reasoner.isConsistent()) {
            System.err.println("Ontology is inconsistent.");
        }
    }

    public void saveOntology() {
        try {
            OWLDocumentFormat format = manager.getOntologyFormat(ontology);
            assert format != null;
            manager.saveOntology(ontology, format, IRI.create(this.destination.toURI()));

//            this.manager.saveOntology(ontology);
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
        }
    }

    private void loadOntology() {
        try {
            this.ontology = manager.loadOntologyFromOntologyDocument(source);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
    }

    private void startReasoner() {
        final ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
        final OWLReasonerConfiguration reasonerConfiguration = new SimpleConfiguration(progressMonitor);
        final ReasonerFactory reasonerFactory = new ReasonerFactory();
        this.reasoner = reasonerFactory.createReasoner(ontology, reasonerConfiguration);
        this.dataFactory = this.manager.getOWLDataFactory();
    }

    private void setupInferenceTypes() {
        final List<InferredAxiomGenerator<? extends OWLAxiom>> axiomGenerators =
                this.getInferredAxiomGenerators();
        this.inferredOntologyGenerator = new InferredOntologyGenerator(this.reasoner, axiomGenerators);
        this.reasoner.precomputeInferences();
    }

    private List<InferredAxiomGenerator<? extends OWLAxiom>> getInferredAxiomGenerators() {
        final List<InferredAxiomGenerator<? extends OWLAxiom>> inferenceGenerators = new ArrayList<>();
        inferenceGenerators.add(new InferredSubClassAxiomGenerator());
        inferenceGenerators.add(new InferredClassAssertionAxiomGenerator());
        inferenceGenerators.add(new InferredDataPropertyCharacteristicAxiomGenerator());
        inferenceGenerators.add(new InferredEquivalentClassAxiomGenerator());
        inferenceGenerators.add(new InferredEquivalentDataPropertiesAxiomGenerator());
        inferenceGenerators.add(new InferredEquivalentObjectPropertyAxiomGenerator());
        inferenceGenerators.add(new InferredInverseObjectPropertiesAxiomGenerator());
        inferenceGenerators.add(new InferredObjectPropertyCharacteristicAxiomGenerator());
        inferenceGenerators.add(new InferredPropertyAssertionGenerator());
//        inferenceGenerators.add(new InferredSubDataPropertyAxiomGenerator());
//        inferenceGenerators.add(new InferredSubObjectPropertyAxiomGenerator());
        inferenceGenerators.add(new InferredDisjointClassesAxiomGenerator());
        return inferenceGenerators;
    }
}
