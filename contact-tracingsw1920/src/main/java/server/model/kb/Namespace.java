package server.model.kb;

public enum Namespace {

    CONTACT_TRACING_NAMESPACE("http://www.semanticweb.org/bonarrigo-rizzato/ontologies/2020/5/contact-tracing#"),
    PERSONA_NAMESPACE("http://blankdots.com/open/personasonto.owl#");

    public final String IRI;

    Namespace(final String IRI) {
        this.IRI = IRI;
    }
}
