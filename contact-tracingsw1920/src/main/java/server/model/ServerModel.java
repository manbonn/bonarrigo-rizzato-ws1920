package server.model;

import domain.Infected;
import domain.OwlDataTransfer;
import server.control.ServerControl;

public interface ServerModel {

    void publishDataTransferObject(OwlDataTransfer event);

    void saveOntology();

    void setController(ServerControl controller);

}
