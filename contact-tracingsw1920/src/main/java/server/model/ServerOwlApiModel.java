package server.model;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import domain.ExposedSubject;
import domain.ExposedSubjectImpl;
import domain.Infected;
import domain.InfectedImpl;
import domain.OwlDataTransfer;
import org.semanticweb.owlapi.model.*;
import server.control.ServerControl;
import server.model.kb.KnowledgeBaseImpl;
import server.model.kb.query.impl.ContactTracingRetriever;
import server.model.kb.query.impl.OWLAdder;
import server.model.kb.query.impl.PersonaRetriever;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ServerOwlApiModel implements ServerModel {

    private ServerControl controller;

    private final KnowledgeBaseImpl knowledgeBase;

    private final ContactTracingRetriever contactTracingRetriever;
    private final PersonaRetriever personaRetriever;
    private final OWLAdder owlAdder;

    public ServerOwlApiModel(final String path){
        this.knowledgeBase = new KnowledgeBaseImpl(path);
        this.contactTracingRetriever = new ContactTracingRetriever(this.knowledgeBase);
        this.personaRetriever = new PersonaRetriever(this.knowledgeBase);
        this.owlAdder = new OWLAdder(this.knowledgeBase);
    }

    @Override
    public void publishDataTransferObject(final OwlDataTransfer dto) {
        this.pushDataToOntology(dto);
        this.knowledgeBase.updateReasoner();
//        this.knowledgeBase.saveOntology();
//        this.mockQuery();
        this.controller.publishInfected(this.getInfectedPeople());
    }

    @Override
    public void saveOntology() {
        this.knowledgeBase.saveOntology();
    }

    private void pushDataToOntology(final OwlDataTransfer dto) {
        final List<OWLDeclarationAxiom> individuals = dto.getIndividuals();
        final List<OWLClassAssertionAxiom> classAssertions = dto.getClassAssertions();
        final List<OWLObjectPropertyAssertionAxiom> objectProperties = dto.getObjectProperties();
        final List<OWLDataPropertyAssertionAxiom> dataProperties = dto.getDataProperties();

        for (final OWLDeclarationAxiom axiom : individuals) {
            this.owlAdder.addAxiom(axiom);
        }
        for (final OWLClassAssertionAxiom axiom : classAssertions) {
            this.owlAdder.addAxiom(axiom);
        }
        for (final OWLObjectPropertyAssertionAxiom axiom : objectProperties) {
            this.owlAdder.addAxiom(axiom);
        }
        for (final OWLDataPropertyAssertionAxiom axiom : dataProperties) {
            this.owlAdder.addAxiom(axiom);
        }

        this.knowledgeBase.flushReasoner();
    }

    @Override
    public void setController(ServerControl controller) {
        this.controller = controller;
        this.controller.publishInfected(this.getInfectedPeople());
    }

    private List<Infected> getInfectedPeople() {
        final List<String> users = this.getIndividualsOfClass("User");

        final List<Infected> result = new ArrayList<>();

        for (final String user : users) {
//            System.out.println("For user " + user);
            final List<String> userTests = this.getObjectPropertyValueOf(user, "testedThrough");

            for (final String test : userTests) {
//                System.out.println("\tFor test " + test);
                final List<String> diagnoses = this.getObjectPropertyValueOf(test, "hasOutcome");
                final List<String> positiveDiagnoses = this.getIndividualsOfClass("Positive");

                for (final String diagnosis : diagnoses) {
                    if (positiveDiagnoses.contains(diagnosis)) {
                        final List<String> circle = this.getObjectPropertyValueOf(user, "sameCircleAs");
                        final List<String> household = this.getObjectPropertyValueOf(user, "livesWith");

                        circle.remove(user);
                        household.remove(user);

                        final List<ExposedSubject> exposedCircleSubjects = circle.stream()
                                .map(s -> createExposedSubjectFrom(s, user, "circle"))
                                .collect(Collectors.toList());
                        final List<ExposedSubject> exposedHouseholdSubjects = household.stream()
                                .map(s -> createExposedSubjectFrom(s, user, "household"))
                                .collect(Collectors.toList());

                        final List<ExposedSubject> exposedSubjects = new ArrayList<>();
                        exposedSubjects.addAll(exposedCircleSubjects);
                        exposedSubjects.addAll(exposedHouseholdSubjects);

                        result.add(createInfectedUserFrom(user, exposedSubjects, test, diagnosis));
                    }
                }
            }
        }

        return result;
    }

    private Infected createInfectedUserFrom(final String user, final List<ExposedSubject> exposedSubjects, final String test, final String diagnosis) {
        final String name = this.getPersonaDataPropertyValueOf(user, "firstName").get(0);
        final String surname = this.getPersonaDataPropertyValueOf(user, "familyName").get(0);

        final String testType = this.findTestType(test);
        final Date testDate = this.dateTimeToDate(this.getDataPropertyValueOf(test, "happenedOn").get(0));
        final Date diagnosisDate = this.dateTimeToDate(this.getDataPropertyValueOf(diagnosis, "happenedOn").get(0));

        return new InfectedImpl(name, surname, testType, testDate, diagnosisDate, exposedSubjects);
    }

    final Date dateTimeToDate(final String dateTime) {
        final StdDateFormat format = new StdDateFormat();
        Date date = null;
        try {
            date = format.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private String findTestType(final String test) {
        final String autopsyClassStr = "Autopsy";
        final String nasoPharingealTestClassStr = "NasoPharingealTest";
        final String fullSerologicalTestClassStr = "FullSerologicalTest";
        final String quickSerologicalTestClassStr = "QuickSerologicalTest";

        final List<String> autopsy = this.getIndividualsOfClass(autopsyClassStr);
        final List<String> nasoPharingeal = this.getIndividualsOfClass(nasoPharingealTestClassStr);
        final List<String> fullSerological = this.getIndividualsOfClass(fullSerologicalTestClassStr);
        final List<String> quickSerological  = this.getIndividualsOfClass(quickSerologicalTestClassStr);

        for (final String t : autopsy) {
            if (test.equals(t)) {
                return autopsyClassStr;
            }
        }
        for (final String t : nasoPharingeal) {
            if (test.equals(t)) {
                return nasoPharingealTestClassStr;
            }
        }
        for (final String t : fullSerological) {
            if (test.equals(t)) {
                return fullSerologicalTestClassStr;
            }
        }
        for (final String t : quickSerological) {
            if (test.equals(t)) {
                return quickSerologicalTestClassStr;
            }
        }

        return null;
    }

    private ExposedSubject createExposedSubjectFrom(final String exposedUser, final String infectedUser, final String exposedType) {
        final String name = this.getPersonaDataPropertyValueOf(exposedUser, "firstName").get(0);
        final String surname = this.getPersonaDataPropertyValueOf(exposedUser, "familyName").get(0);
        final String telephoneNumber = this.getDataPropertyValueOf(exposedUser, "telephoneNumber").get(0);
        final Date date = this.getEncounterDate(infectedUser, exposedUser);
        return new ExposedSubjectImpl(name, surname, telephoneNumber, date, exposedType);
    }

    private Date getEncounterDate(final String firstUser, final String secondUser) {
        final List<String> encounters = this.getObjectPropertyValueOf(firstUser, "madeEncounter");
//        System.out.println("madeEncounter " + encounters);

        final String dateStr = encounters.stream()
                .filter(e -> {
                    final List<String> madeBy = this.getObjectPropertyValueOf(e, "madeBy");
//                    System.out.println("madeBy " + madeBy);
                    return madeBy.contains(secondUser);
                })
                .map(e -> {
                    String happenedOn = this.getDataPropertyValueOf(e, "happenedOn").get(0);
//                    System.out.println("happenedOn " + happenedOn);
                    return happenedOn;
                })
                .findFirst()
                .orElse("2020-06-21T17:00:00");

        final StdDateFormat format = new StdDateFormat();
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    private void mockQuery() {
        this.knowledgeBase.updateReasoner();
        final List<String> userNames = this.namesFrom(this.contactTracingRetriever.getIndividualsOfClass("User"));
        System.out.println(userNames);

        final List<String> others = this.namesFrom(
                this.contactTracingRetriever.getObjectPropertyValue("Zeno_Zani", "sameCircleAs")
        );
        System.out.println(others);
    }

    private List<String> getIndividualsOfClass(final String className) {
        return this.namesFrom(this.contactTracingRetriever.getIndividualsOfClass(className));
    }

    private List<String> getObjectPropertyValueOf(final String individualName, final String propertyName) {
        return this.namesFrom(this.contactTracingRetriever.getObjectPropertyValue(individualName, propertyName));
    }

    private List<String> getDataPropertyValueOf(final String individualName, final String propertyName) {
        return this.valuesFrom(this.contactTracingRetriever.getDataPropertyValue(individualName, propertyName));
    }

    private List<String> getPersonaDataPropertyValueOf(final String individualName, final String propertyName) {
        return this.valuesFrom(this.personaRetriever.getPersonaDataPropertyValue(individualName, propertyName));
    }

    private List<String> namesFrom(final List<OWLNamedIndividual> queryResult) {
        return queryResult.stream()
                .map(OWLNamedIndividual::getIRI)
                .map(IRI::getFragment)
                .collect(Collectors.toList());
    }

    private List<String> valuesFrom(final List<OWLLiteral> queryResult) {
        return queryResult.stream()
                .map(OWLLiteral::getLiteral)
                .collect(Collectors.toList());
    }
}
