package server.view;

import domain.ExposedSubject;
import domain.Infected;
import server.Server;
import server.control.ServerControl;

import static server.Server.ServerMonitorPosition;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.net.URI;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ServerSwingView extends JFrame implements ServerView {

    private static final Integer[] DAYS_THRESHOLD =
            new Integer[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private static final String[] MONTH =
            new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September",
                    "October", "November", "December"};
    private static final Integer[] YEARS = new Integer[]{2018, 2019, 2020, 2021};

    private static final double WIDTH_PERCENTAGE = 0.7;
    private static final double HEIGHT_PERCENTAGE = 1;

    private Date dateThreshold = new Date();

    private java.util.List<Infected> infectedList = new ArrayList<>();

    private static final Rectangle MONITOR =
            GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getDefaultScreenDevice()
                .getDefaultConfiguration()
                .getBounds();

    private static final int WIDTH = (int) ( MONITOR.width * WIDTH_PERCENTAGE);
    private static final int HEIGHT = (int) ( MONITOR.height * HEIGHT_PERCENTAGE);

    private ServerControl controller;

    private final JPanel mainPanel = new JPanel();

    private JPanel tablePanel;
    private Component circleTable;

    public ServerSwingView(final ServerMonitorPosition position){
        initViewStructure();
        initViewState();
        initViewRendering();
        locatePosition(position);
    }

    @Override
    public void publishInfected(final java.util.List<Infected> infections) {
        synchronized (this.infectedList) {
            this.infectedList = infections;
        }
        this.updateInfectedTable();
    }

    private void updateInfectedTable() {
        SwingUtilities.invokeLater( () -> {
            mainPanel.remove(tablePanel);
            tablePanel = createDataTables(this.infectedList);
            mainPanel.add(tablePanel, BorderLayout.CENTER);
            this.revalidate();
        });
    }

    @Override
    public void setController(ServerControl controller) {
        this.controller = controller;
    }

    private JPanel createDataTables(java.util.List<Infected> infections) {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0, 8)));
        panel.add(createSidebar());
        panel.add(Box.createRigidArea(new Dimension(0, 8)));

        panel.add(createInfectedDataTable(infections));
        if (infections.isEmpty()) {
            circleTable = createCircleDataTable(new ArrayList<>());
        } else {
            circleTable = createCircleDataTable(infections.get(0).circle());
        }
        panel.add(circleTable);

        return panel;
    }

    private JScrollPane createInfectedDataTable(java.util.List<Infected> infections) {
        TableModel dataModel = new InfectedTableModel(this.getFilteredInfectedList(infections));

        JTable table = new JTable(dataModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        table.getSelectionModel().addListSelectionListener(e -> {
            int[] selectedRow = table.getSelectedRows();


            SwingUtilities.invokeLater( () -> {
                tablePanel.remove(circleTable);
                circleTable = createCircleDataTable(infections.get(selectedRow[0]).circle());
                tablePanel.add(circleTable);
                this.revalidate();
            });
        });

        return new JScrollPane(table);
    }

    private java.util.List<Infected> getFilteredInfectedList(final java.util.List<Infected> infectedList) {
        return infectedList.stream()
                .filter(i -> {
                    final Date diagnosisDate = i.diagnosisDate();
                    return diagnosisDate.before(this.dateThreshold);
                })
                .collect(Collectors.toList());
    }

    private JScrollPane createCircleDataTable(java.util.List<ExposedSubject> circle) {
        TableModel dataModel = new CircleTableModel(circle);

        JTable table = new JTable(dataModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        return new JScrollPane(table);
    }

    private JPanel createSidebar() {
        final JPanel sidebar = new JPanel();

        final JButton viewOntology = new JButton("View Ontology");
        final JButton saveInferences = new JButton("Save Inferences");

        sidebar.setLayout(new BoxLayout(sidebar, BoxLayout.X_AXIS));

        sidebar.add(createDateSelector());
        sidebar.add(Box.createRigidArea(new Dimension(70, 0)));
        sidebar.add(viewOntology);
        sidebar.add(Box.createRigidArea(new Dimension(15, 0)));
        sidebar.add(saveInferences);
        sidebar.add(Box.createRigidArea(new Dimension(15, 0)));

        viewOntology.addActionListener(e -> this.showOntology());

        saveInferences.addActionListener(e -> controller.saveOntology());

        return sidebar;
    }

    private void showOntology() {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI("http://www.visualdataweb.de/webvowl/#iri=https://manbonn.gitlab.io/bonarrigo-rizzato-ws1920/contact-tracing-empty.owl"));
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JPanel createDateSelector() {
        final Integer[] initialDays = Arrays.stream(IntStream.rangeClosed(1, 31).toArray()).boxed().toArray(Integer[]::new);

        final JPanel selector = new JPanel();
        selector.setLayout(new BoxLayout(selector, BoxLayout.X_AXIS));

        final JComboBox<Integer> daySelector = new JComboBox<>(initialDays);
        final JComboBox<String> monthSelector = new JComboBox<>(MONTH);
        final JComboBox<Integer> yearSelector = new JComboBox<>(YEARS);
        final JButton confirm = new JButton("Confirm");

        daySelector.setSelectedItem(this.dateThreshold.getDate());
        monthSelector.setSelectedItem(MONTH[this.dateThreshold.getMonth()]);
        yearSelector.setSelectedItem(this.dateThreshold.getYear());

        selector.add(Box.createRigidArea(new Dimension(15, 0)));
        selector.add(daySelector);
        selector.add(Box.createRigidArea(new Dimension(15, 0)));
        selector.add(monthSelector);
        selector.add(Box.createRigidArea(new Dimension(15, 0)));
        selector.add(yearSelector);
        selector.add(Box.createRigidArea(new Dimension(15, 0)));
        selector.add(confirm);
        yearSelector.setSelectedIndex(2);

        monthSelector.addActionListener(e -> {
            final JComboBox<String> source = (JComboBox<String>) e.getSource();
            final int days = DAYS_THRESHOLD[source.getSelectedIndex()];
            daySelector.removeAllItems();
            if(((Integer) yearSelector.getSelectedItem() % 4 == 0) && monthSelector.getSelectedIndex() == 1) {
                for(Integer day : IntStream.rangeClosed(1, days + 1).boxed().collect(Collectors.toList())){
                    daySelector.addItem(day);
                }
            } else {
                for(Integer day : IntStream.rangeClosed(1, days).boxed().collect(Collectors.toList())){
                    daySelector.addItem(day);
                }
            }
        });

        yearSelector.addActionListener(e -> {
            final JComboBox<Integer> source = (JComboBox<Integer>) e.getSource();
            if((Integer) source.getSelectedItem() % 4 == 0 && monthSelector.getSelectedIndex() == 1) {
                monthSelector.setSelectedIndex(1);
            }
        });

        confirm.addActionListener(e -> {
            final int year = ((Integer) yearSelector.getSelectedItem()) - 1900;
            final int month = monthSelector.getSelectedIndex();
            final Integer day = (Integer) daySelector.getSelectedItem();
            final Date date = new Date(year, month, day);

            this.dateThreshold = date;

            this.updateInfectedTable();

//            controller.filterDate(date);
        });

        return selector;
    }

    private void initViewState() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initViewRendering() {
        this.setSize(WIDTH, HEIGHT);
        this.setTitle("Reasoner Server");

        this.setVisible(true);
    }

    private void locatePosition(Server.ServerMonitorPosition position) {
        switch(position){
            case LEFT: {
                int x = 0;
                int y = 0;
                this.setLocation(x, y);
                break;
            }
            case RIGHT: {
                int x = (int) MONITOR.getMaxX() - this.getWidth();
                int y = (int) MONITOR.getMaxY() - this.getHeight();
                this.setLocation(x, y);
                break;
            }
        }
    }

    private void initViewStructure() {
        mainPanel.setLayout(new BorderLayout());

        tablePanel = createDataTables(new ArrayList<>());
        mainPanel.add(tablePanel, BorderLayout.CENTER);

        this.getContentPane().add(mainPanel);
    }

    private static class CircleTableModel extends AbstractTableModel {

        private final String[] columnNames = {"Name", "Surname", "Telephone", "Encounter Date", "Encounter type"};
        private final Object[][] data;

        public CircleTableModel(java.util.List<ExposedSubject> circle){
            data = new Object[circle.size()][5];
            int counter = 0;
            for (ExposedSubject i : circle) {
                Object [] newRow = new Object[] {
                        i.name(),
                        i.surname(),
                        i.telephone(),
                        i.encounterDate(),
                        i.encounterType()
                };
                data[counter++] = newRow;
            }
        }

        @Override
        public int getRowCount() {
            return data.length;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return data[rowIndex][columnIndex];
        }

        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }

    }

    private static class InfectedTableModel extends AbstractTableModel {

        private final String[] columnNames = {"Name", "Surname", "Test Type", "Test Date", "Diagnosis Date"};
        private final Object[][] data;

        public InfectedTableModel(java.util.List<Infected> infections){
            data = new Object[infections.size()][5];
            int counter = 0;
            for (Infected i : infections) {
                Object [] newRow = new Object[] {
                        i.name(),
                        i.surname(),
                        i.testType(),
                        i.testDate(),
                        i.diagnosisDate()
                };
                data[counter++] = newRow;
            }
        }

        @Override
        public int getRowCount() {
            return data.length;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return data[rowIndex][columnIndex];
        }

        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }

    }


}
