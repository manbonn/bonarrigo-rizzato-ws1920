package server.view;

import domain.Infected;
import server.control.ServerControl;

import java.util.List;

public interface ServerView {

    void publishInfected(List<Infected> infections);

    void setController(ServerControl controller);

}
