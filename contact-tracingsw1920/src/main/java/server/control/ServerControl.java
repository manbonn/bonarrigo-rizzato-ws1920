package server.control;

import domain.Infected;
import server.model.ServerModel;
import server.view.ServerView;

import java.util.Date;
import java.util.List;

public interface ServerControl {

    void attachView(ServerView view);

    void filterDate(Date date);

    void publishInfected(List<Infected> infectedList);

    void saveOntology();

    void setModel(ServerModel model);

}
