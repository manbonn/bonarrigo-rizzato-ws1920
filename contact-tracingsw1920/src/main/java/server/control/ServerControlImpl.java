package server.control;

import domain.Infected;
import domain.OwlDataTransfer;
import server.model.ServerModel;
import server.view.ServerView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.List;

public class ServerControlImpl implements ServerControl {

    private ServerModel model;
    private ServerView view = null;

    public ServerControlImpl() {
        new ListeningAgent().start();
    }

    private synchronized void publishClientData(final OwlDataTransfer dto) {
        if(view == null) {
            throw new IllegalStateException("No view is attached to the ServerControl");
        }
        model.publishDataTransferObject(dto);
    }

    @Override
    public void attachView(final ServerView view) {
        this.view = view;
    }

    @Override
    public void filterDate(final Date date) {
        System.out.println(date);
    }

    @Override
    public void publishInfected(List<Infected> infectedList) {
        view.publishInfected(infectedList);
    }

    @Override
    public void saveOntology() {
        this.model.saveOntology();
    }

    @Override
    public void setModel(ServerModel model) {
        this.model = model;
    }

    private class ListeningAgent extends Thread {

        private final static int LISTENING_PORT = 8888;

        @Override
        public void run() {
            ServerSocket serverSocket = null;
            Socket clientSocket = null;

            try {
                serverSocket = new ServerSocket(LISTENING_PORT);
            } catch (IOException e) {
                e.printStackTrace();
            }

            while(true) {
                try {
                    clientSocket = serverSocket.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                new CommunicatingAgent(clientSocket).start();
            }
        }
    }

    private class CommunicatingAgent extends Thread {

        private final Socket socket;

        public CommunicatingAgent(final Socket socket) {
            this.socket = socket;
        }

        public void run() {
            ObjectInputStream input = null;

            try {
                input = new ObjectInputStream(socket.getInputStream());
                OwlDataTransfer e = (OwlDataTransfer) input.readObject();
                publishClientData(e);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
