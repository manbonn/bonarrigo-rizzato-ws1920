package server;

import server.control.ServerControl;
import server.control.ServerControlImpl;
import server.model.ServerModel;
import server.model.ServerOwlApiModel;
import server.view.ServerSwingView;
import server.view.ServerView;

public class Server {

    public static void main(String[] args) {
        final String ontology = args[0];
        final ServerMonitorPosition position = decodePositionalArgument(args[1]);

        final ServerModel model = new ServerOwlApiModel(ontology);
        final ServerControl controller = new ServerControlImpl();
        final ServerView view = new ServerSwingView(position);

        controller.setModel(model);
        controller.attachView(view);
        model.setController(controller);
        view.setController(controller);
    }

    private static ServerMonitorPosition decodePositionalArgument(final String position){
        switch (position){
            case "left": {
                return ServerMonitorPosition.LEFT;
            }
            case "right": {
                return ServerMonitorPosition.RIGHT;
            }
            default: {
                throw new IllegalArgumentException("Only left and right values are accepted");
            }
        }
    }

    public enum ServerMonitorPosition {
        LEFT, CENTER, RIGHT
    }

}
