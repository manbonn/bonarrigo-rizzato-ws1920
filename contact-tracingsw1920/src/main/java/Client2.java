import client.Client;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Client2 {

    private final static String CLIENT_ID = "2";
    private final static String CLIENT_POSITION = "bottomleft";
    private final static Path RDF =
            Paths.get(System.getProperty("user.dir"), "src", "main", "resources", "sourceClient2.ct");

    public static void main(String[] args) {
        final String[] firstClientArgs = new String[] {CLIENT_ID, RDF.toString(), CLIENT_POSITION};
        Client.main(firstClientArgs);
    }

}